(function() {
	'use strict';

	var isDlgOpen;

	angular.module('calendar', ['ngDialog', 'ngMaterial'])
		.controller('Calendar', ['$scope', 'ngDialog', '$mdToast', Calendar])
	;

function Calendar($scope, ngDialog, $mdToast) {

	var vm = this;

	let watch = false;
	let firstElemt;
	let days = [];

	document.onmousemove = moveMouse;
	document.onmouseup = function() {
		console.log('mouseup')
		watch = false;

		if(days.length > 0 && !ngDialog.isOpen() ) {
			console.log(days);

ngDialog.open({
	template: (days.length===1) ? '/ng-templates/calendar/create-one-day-event.html' : '/ng-templates/calendar/create-multi-days-event.html',
	className: 'ngdialog-theme-default',
	controller: ['$scope', '$http', function($scope, $http) {
		let that = this;

		let pDays = days;
		days = [];

		that.slot = {start: pDays[0], end: pDays.pop()};
		that.saving = false;

		that.save = function() {
			console.log(that.slot);
			that.saving = true;

			ngDialog.close();
			// showToast();

			// Do save
			
			var req = {
				method: 'POST',
				url: '/api/v1/planning',
				headers: {
					'Content-Type': 'application/json'
				},
				data: that.slot
			};

			$http(req)
			 .then(function(data) {
				 console.log(data);
				 window.location.reload();
			 }, function(error) {
				 console.error(error);
			 });
			
		}

		var last = {
			bottom: false,
			top: true,
			left: false,
			right: true
		};
		
		$scope.toastPosition = angular.extend({},last);

		$scope.getToastPosition = function() {
			sanitizePosition();

			return Object.keys($scope.toastPosition)
				.filter(function(pos) { return $scope.toastPosition[pos]; })
				.join(' ');
		};

		function sanitizePosition() {
			var current = $scope.toastPosition;

			if ( current.bottom && last.top ) current.top = false;
			if ( current.top && last.bottom ) current.bottom = false;
			if ( current.right && last.left ) current.left = false;
			if ( current.left && last.right ) current.right = false;

			last = angular.extend({},current);
		}

		/*function showToast() {
			var pinTo = $scope.getToastPosition();
			var toast = $mdToast.simple()
			.textContent('Marked as read')
			.action('UNDO')
			.highlightAction(true)
			.position(pinTo);

			$mdToast.show(toast).then(function(response) {
				if ( response == 'ok' ) {
					alert('You clicked the \'UNDO\' action.');
				}
			});
		}*/
		
	}],
	controllerAs: 'ctrl',
	preCloseCallback: function() {
		var node = document.getElementById('overlay-container');
		while (node.firstChild) {
			node.removeChild(node.firstChild);
		}
	}
});

		}
	};


	let nodeList = document.querySelectorAll('[data-workout]');
	let callback = function(evt) {
		evt.preventDefault();
		console.log('callback',evt);

ngDialog.open({
	template: '/ng-templates/calendar/event.html',
	className: 'ngdialog-theme-default',
	controller: ['$scope', '$http', function($scope, $http) {
		$scope.loading = true;

		$http.get('/api/v1/workout/'+evt.target.parentElement.attributes['data-workout'].value)
		 .then(function(response) {
			 console.log('sukes', response);
			 $scope.workout = response.data;
			 $scope.loading = false;
		 }, function() {
			 console.log('pas sukes');

		 });
		
	}]
});

return;
		let bubble = evt.target.parentNode.querySelector('.workout-bubble').cloneNode(true);
		
		var offset = getOffset(evt.target);
		bubble.style = "left:"+(offset.left-50)+"px;top:"+(offset.top-50)+"px;";

		document.getElementById('bubble').innerHTML = '';
		document.getElementById('bubble').appendChild(bubble);

		// Ajout d'un listener sur la fermture de la bubble TODO html
		bubble.querySelector('.close').addEventListener('click', function(evt) { document.getElementById('bubble').innerHTML = ''; }, false);	

	}
	console.log(nodeList);
	addEventListenerList(nodeList, 'click', callback);

	function addEventListenerList(list, event, fn) {
		for (var i = 0, len = list.length; i < len; i++) {
			list[i].addEventListener(event, fn, false);
		}
	}


	/*
	 * controller methods
	 */
	vm.click = function(day) {
		console.log(day);

	}

	vm.mousedown = function(evt, cell, day) {
		console.log('MousePos=',getMousePos(evt));
		console.log('evt=',evt);
		
		var el = evt.target;
		if(!el.classList.contains('day')) return;

		firstElemt = el;

		drawOverlays([el]);

		watch = true;

		// Détecter le jour et le garder en mémoire
		days.push(day);


	}

	function elemtBetween(a, b) {

		var from, to;
		var aOff = getOffset(a);
		var bOff = getOffset(b);

		if(aOff.top < bOff.top) {
			from = a;
			to = b;
		} else if(aOff.top == bOff.top) {
			if(aOff.left < bOff.left) {
				from = a;
				to = b;
			} else {
				from = b;
				to = a;
			}
		} else {
			from = b;
			to = a;
		}

		var matched = [from];

		while ( ( from = from.nextElementSibling ) && from.nodeType !== 9 ) {
			if ( from.nodeType === 1 ) {
				if ( from.isSameNode(to) ) {
					break;
				}
				if(from.classList.contains('day')) {
					matched.push( from );
				}
				
				if( !from.nextElementSibling || !from.nextElementSibling.nextElementSibling ) {
					// debugger;
					from = from.parentElement.nextElementSibling.firstElementChild;
					console.log(from);
				}
			}
		}
		matched.push(to);

		return matched;
	};

	/*
	 * Private functions
	 */
	function moveMouse(evt) {
		if(!watch) return;
		// Préviens la "sélection à la volée" des données dans les éléments html
		evt.preventDefault();

console.log(evt.target);

		var el = evt.target.closest('td');

		if(el && el.classList.contains('day')) {
			var els = elemtBetween(firstElemt, el);
			drawOverlays(els);
			days = [];
			for(var i=0;i<els.length;i++) {
				days.push(els[i].attributes['data-day'].value);
			}

		}

		var pos = getMousePos(evt);

		if((pos.y-window.scrollY) < 1 && window.scrollY > 0) {
			window.scrollBy(0,pos.y-window.scrollY);
			pos = getMousePos(evt);
		}

		if( pos.y >= window.innerHeight ) {
			// TODO FIX ugly 
			window.scrollBy(0,(pos.y-window.innerHeight)/10);
			pos = getMousePos(evt);
		}

		//if(pos.y >= (taille de la fenêtre + scrollY) && (document.ysize - taille fenetre) != scrollY)

		console.log("X: " + pos.x);
		console.log("Y: " + pos.y);

	}

	function drawOverlays(els) {

		var node = document.getElementById('overlay-container');
		while (node.firstChild) {
			node.removeChild(node.firstChild);
		}

		var el;
		for(var i=0; i<els.length; i++){
			el = els[i];
			var offset = getOffset(el);

			var overlayElmt = document.createElement('div');
			overlayElmt.style = "left:"+offset.left+"px;top:"+offset.top+"px;width:"+el.offsetWidth+"px;height:"+el.offsetHeight+"px;";
			overlayElmt.innerHTML = '&nbsp;';

			node.appendChild(overlayElmt);
		}
	}

	function getMousePos(evt) {
		var doc = document.documentElement || document.body;
		var pos = {
			x: evt ? evt.pageX : window.event.clientX + doc.scrollLeft - doc.clientLeft,
			y: evt ? evt.pageY : window.event.clientY + doc.scrollTop - doc.clientTop
		};
		return pos;

	}

	function getOffset( el ) {
		var _x = 0;
		var _y = 0;
		while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
			_x += el.offsetLeft;// + el.scrollLeft;
			_y += el.offsetTop;// + el.scrollTop;
			el = el.offsetParent;
		}
		return { top: _y, left: _x };
	}



}

if (window.Element && !Element.prototype.closest) {
    Element.prototype.closest = 
    function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i,
            el = this;
        do {
            i = matches.length;
            while (--i >= 0 && matches.item(i) !== el) {};
        } while ((i < 0) && (el = el.parentElement)); 
        return el;
    };
}

})();