<?php

namespace SylvainLG\Training;

/**
 * Fonctions utilisable dans Twig
 *  Les fonctions sont accessibles directement sous la forme
 * 		{{ function(param) }}
 */
class TwigExtension extends \Twig_Extension implements ContainerAwareInterface {

	use ContainerAwareTrait;

	public function getFunctions() {
		return [
			new \Twig_SimpleFunction('route', [$this, 'route']),
		];
	}
	
	public function getGlobals() {
		return [
			'app' => [
				'session' => $_SESSION,
			]
		];
	}

	public function getFilters() {
		return [
			new \Twig_SimpleFilter('bsonutcdate', function ($bsonutcdate, $format) {
				if(!$bsonutcdate instanceof \MongoDB\BSON\UTCDateTime) {
					return $bsonutcdate;
				}
				return htmlentities($bsonutcdate->toDateTime()->format($format));
			}),
			new \Twig_SimpleFilter('mongodate', function ($date, $format) {
				if(!$date instanceof \MongoDB\BSON\UTCDateTime) {
					return $date;
				}
				return htmlentities($date->toDateTime()->format($format));
			}),
			new \Twig_SimpleFilter('fillblank', function ($string) {
				return preg_replace('/[^a-zA-Z0-9]/', '-', $string);
			}),
		];
	}

	/**
	 * Permet de faire du reverse routing directement 
	 * dans les templates en utilisant le moteur Phroute
	 * 
	 * Le slash ajouté au return est un hack, le router devrait le retourner ...
	 *  TODO Déclarer le problème sur Github
	 * 
	 * @see https://github.com/mrjgreen/phroute Named Routes for Reverse Routing
	 */ 
	public function route($route, $args = null) {
		$this->_container['log']->debug('twig_extension_route', ['__METHOD__'=>__METHOD__, 'route'=>$route, 'args'=>$args]);
		$router = $this->_container['router'];
		if(!$router->hasRoute($route)) {
			throw new \Exception('Route not found in template');
		}
		return '/'.$router->route($route, $args);
	}

	public function getName() {
		return 'sylvainlg_extension';
	}

}