<?php

namespace SylvainLG\Training\Controller;

class ActivityController extends BaseController {

	/**
	 * Emet un évènement sur MQ pour demander au worker
	 * de faire une mise à jour pour ce user.
	 */
	public function sync() {
		
		// TODO: Trouve une solution pour suivre l'execution de la mise à jour.

		$activityService = $this->_container['activity'];
		$activities = $activityService->syncWithStrava();

		return $this->render('activity/sync.html.twig', []);
	}

	/**
	 * Liste des activités
	 * Pagination par 25
	 */
	public function list() {

		$this->_log->info('list', ['__METHOD__'=>__METHOD__], []);

		$activities = $this->_container['mysql']->query('SELECT uuid, id, name, start_date, trimp, 0 '.
		'FROM training_activity ORDER BY start_date DESC LIMIT 25');

		return $this->render('activity/list.html.twig', ['activities'=>$activities]);
	}

	public function detail($id) {

		$activityService = $this->_container['activity'];
		$api = $this->_container['api'];

		$activity = $activityService->get($id);

		if($activity->type == 'Ride') {

			$detail = $activityService->detail($id);

			$params = [
				'type' => $activity->type,
				'cumul' => $activity->moving_time,
				'formatted_cumul' => gmdate('H:i:s', $activity->moving_time),
				// 'coef' => $coef,
				//'precent_reussite' => $detail['moyenne'],
				'detail' => json_encode($detail),
			];

			if(isset($activity->scores) && !is_string($activity->scores)) {
				$params['TRIMS'] = $activity->scores->TRIMP;
				$params['isie'] = $activity->scores->isie;
			}

			// $act = json_decode($activity->raw);
			// $params['polyline'] = $act->map->polyline;
			// TODO
			// http://leafletjs.com/reference-1.2.0.html#polyline
			// https://github.com/jieter/Leaflet.encoded

			return $this->render(
				'activity/detail.html.twig', 
				$params
			);

		} else {

			return $this->render(
				'activity/detail.html.twig', 
				[
					'type' => $activity->type,
					'cumul' => $activity->moving_time,
					'formatted_cumul' => gmdate('H:i:s', $activity->moving_time),
				]
			);

		}

	}

	public function resync($id) {
		$this->_container['activity']->syncOneWithStrava($id);

		$this->redirect('activity_detail', [$id]);
	}


	/**
	 * Affichage de nombreux charts
	 */
	public function charts() {

		$this->_container['log']->debug('charts', ['__METHOD__' => __METHOD__]);

		return $this->render('activity/charts.html.twig', [
			'trimps' => $this->_container['activity']->trimps(),
			'weekly_trimps' => $this->_container['activity']->weekly_trimps(),
			'monthly_trimps' => $this->_container['activity']->monthly_trimps(),
			'activitiesCount' => $this->_container['activity']->count()
		]);
	}

}