<?php

namespace SylvainLG\Training\Controller;

abstract class BaseController implements \SylvainLG\Training\ContainerAwareInterface {

	use \SylvainLG\Training\ContainerAwareTrait {
		setContainer as traitSetContainer;
	}

	/**
	 * Le Logger de l'application
	 *
	 *  => Utile : debug_backtrace()[1]['function']; ?
	 */
	protected $_log;

	/**
	 * @Override 
	 */
	public function setContainer(\Pimple\Container $container) {
		$this->traitSetContainer($container);

		$this->_log = $this->_container['log'];
		$this->_log->info('Start controller', [debug_backtrace()[1]['class']]);
	}

	protected function render($view, array $parameters = array()) {
		return $this->_container['twig']->render($view, $parameters);
	}

	protected function redirect($route, $args) {
		$this->_container['log']->debug('redirect', ['__METHOD__'=>__METHOD__], [$route, $args]);
		$to = '/'.$this->_container['router']->route($route, $args);
		header("Location: " . $to);
		exit;
	}

}