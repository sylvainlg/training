<?php

namespace SylvainLG\Training\Controller\Api;


/**
 * BaseRestController
 *
 * Fonctions de base pour un controller de type Rest
 *  LE FONCTIONNEMENT NORMAL DE LA BRIQUE PHROUTE EST HACKEE CAR NON FONCTIONNELLE SUR DU REST PROPRE
 * 
 * Nous y retrouvons les fonctionnalités de base d'un controller normal
 * Plus :
 * 		- Fonction de formatage du contenu
 * 		- Fonction raccourcie pour formatter la réponse en json
 * 
 * @see https://github.com/mrjgreen/phroute#controllers
 * @see \SylvainLG\Training\Controller\BaseController
 * @author Sylvain LE GLEAU <syl@sylvainlg.fr>
 */
abstract class BaseRestController extends \SylvainLG\Training\Controller\BaseController {

	/**
	 * Cette méthode surcharge le fonctionnement normal de Phroute
	 * Elle permet de faire du Rest avec les bons points d'entrée
	 */
	public function anyIndex($id = null) {

		$method = $_SERVER['REQUEST_METHOD'];
		$this->_container['log']->info('any', ['__METHOD__'=>__METHOD__, 'method'=>$method, 'id'=>$id]);

		switch($method) {
			case 'GET':
				return $this->_get($id);
				break;
			case 'POST':
				return $this->_post();
				break;
			case 'PUT':
				return $this->_put($id);
				break;
			case 'DELETE':
				return $this->_delete($id);
				break;
			default:
				header('HTTP/1.1 400 Bad Request');
				exit;
		}

	}

	abstract protected function _get($id);

	abstract protected function _post();

	abstract protected function _put($uuid);

	abstract protected function _delete($uuid);

	/**
	 * __format
	 *
	 * @param $response mixed Réponse à formatter
	 * @return mixed la réponse formattée
	 * @return HTTP 406
	 */
	protected function _format($response) {
		$this->_container['log']->debug('_format', ['__METHOD__'=>__METHOD__]);

		// TODO improve

		$supported = ['text/json'];

		if($this->_container['accept']->isAccepted('text/json')) {
			return $this->_json($response);
		} else {
			$this->_container['log']->warning('HTTP Accept header is not supported', ['__METHOD__'=>__METHOD__], [$_SERVER['HTTP_ACCEPT']]);
			header('HTTP/1.1 406 Not Acceptable');
			exit;
		}

	}

	/**
	 * _json formatte la réponse en json
	 *
	 * @param $response mixed
	 * @return string json
	 */
	protected function _json($response) {
		$this->_container['log']->debug('_json', ['__METHOD__'=>__METHOD__]);
		header('Content-Type: application/json');
		return json_encode($response);
	}

}