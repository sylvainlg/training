<?php

namespace SylvainLG\Training\Controller\Api;

/**
 * Api Workout
 * 
 * Expose une API Rest à la manière de Phroute
 *
 * @see BaseRestController
 * @author Sylvain LE GLEAU <syl@sylvainlg.fr>
 */
class WorkoutRestController extends BaseRestController {

	/**
	 * getIndex
	 * 
	 * Expose le / et /index en get
	 * 
	 * @param optionnal workout UUID
	 * @return mixed le workout ou la liste des workouts
	 */
	protected function _get($workoutUuid) {
		$this->_container['log']->debug('get', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);

		if($workoutUuid === null) {
			$workoutService = $this->_container['workout'];
			return $this->_format($workoutService->all());
		} elseif(!is_string($workoutUuid)) {
			$this->_container['log']->warning('Requested workout UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
			header('HTTP/1.1 400 Bad request');
			exit;
		} else {

			$workout = $this->_container['workout']->get($workoutUuid);
			if(is_null($workout)) {
				$this->_container['log']->warning('Requested workout not found', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
				header('HTTP/1.1 404 Not found');
				exit;
			} else {
				$workout->date = (new \Datetime($workout->date))->format('U') * 1000;
				return $this->_format($workout);
			}

		}

	}

	/**
	 * postIndex
	 * HTTP POST
	 *
	 * Créé un nouveau workout
	 * 
	 * @return mixed well formatted workout
	 */
	protected function _post() {
		$this->_container['log']->debug('post', ['__METHOD__'=>__METHOD__]);

		/*
		 * Création de l'objet workout
		 * Vérfication des données
		 * Sauvegarde du workout
		 */
		$w = new \SylvainLG\Training\Model\Workout();

		$filtered_post = $this->parseRequest();

		$w->title 		= $filtered_post['title'];
		$w->date 		= $filtered_post['date']; //new \DateTime($filtered_post['date'].' 00:00:00');
		$w->type 		= $filtered_post['type'];
		$w->content 	= $filtered_post['content'];
		$w->notes 		= '';
		$w->duration 	= '';
		$w->difficulty 	= '';
		$w->steps 		= $filtered_post['steps'];

		$workoutService = $this->_container['workout'];
		$workoutService->add($w);

		http_response_code(201);
		return $this->_format($w);

	}

	/**
	 * putIndex
	 * 
	 * HTTP PUT
	 * 
	 * @param $workoutUuid
	 * @return mixed formatted and updated workout
	 */
	protected function _put($workoutUuid) {

		/*
		 * Le uuid est-il présent
		 */
		if(is_null($workoutUuid) or !is_string($workoutUuid)) {
			$this->_container['log']->warning('Requested workout UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
			header('HTTP/1.1 400 Bad request');
			exit;
		}

		/*
		 * Récupération du workout
		 * Vérification que celui-ci existe
		 */
		$workoutService = $this->_container['workout'];
		$workout = $workoutService->get($workoutUuid);
		if(is_null($workout)) {
			$this->_container['log']->warning('Requested workout not found', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
			header('HTTP/1.1 404 Not found');
			exit;
		}

		/*
		 * Do the work
		 */

		$filtered_post = $this->parseRequest();

		$workout->title 		= $filtered_post['title'];
		$workout->date 		= new \DateTime($filtered_post['date'].' 00:00:00');
		$workout->type 		= $filtered_post['type'];
		$workout->content 	= $filtered_post['content'];
		// $workout->notes 		= '';
		// $workout->duration 	= '';
		// $workout->difficulty 	= '';
		$workout->steps 		= $filtered_post['steps'];

		$workoutService->set($workout);

		return $this->_format($workout);

	}


	 /**
	  * deleteIndex
	  * HTTP DELETE
	  *
	  * @param $workoutUuid
	  */
	protected function _delete($workoutUuid) {
		$this->_container['log']->debug('delete', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
		//workout_583cb75b0af219.55136114

		if(is_null($workoutUuid) or !is_string($workoutUuid)) {
			$this->_container['log']->warning('Requested workout UUID is not a string', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
			header('HTTP/1.1 400 Bad request');
			exit;
		} else {

			$workout = $this->_container['workout']->get($workoutUuid);
			if(is_null($workout)) {
				$this->_container['log']->warning('Requested workout not found', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
				header('HTTP/1.1 404 Not found');
				exit;
			} else {
				if($this->_container['workout']->delete($workout)) {
					$this->_container['log']->debug('Workout is deleted', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
					header('HTTP/1.1 204 No Content');
					exit;
				} else {
					$this->_container['log']->error('Workout is not deleted', ['__METHOD__'=>__METHOD__, 'id'=>$workoutUuid]);
					header('HTTP/1.1 500 Internal Server Error');
					exit;
				}
			}

		}
	}

	/**
	 * Fait le boulot long et chiant de vérification des inputs
	 * Elle doit être utilisée par POST et PUT
	 * 
	 * @return array les inputs tous propres
	 */
	private function parseRequest() {

		$this->_container['log']->debug('parseRequest', ['__METHOD__'=>__METHOD__]);

		$post = [];

		/*
		 * Récupère le contenu de la requête post depuis le body
		 */
		$entityBody = file_get_contents('php://input');
		if(!empty($entityBody)) {

			/*
			 * Récupère et analyse le Content-Type
			 * Seul le application/json est actuellement accepté
			 * Sinon HTTP 415
			 */
			$contentType = $_SERVER['CONTENT_TYPE'];
			if($contentType !== 'application/json') {
				$this->_container['log']->warning('Post data are not application/json type ', ['__METHOD__'=>__METHOD__, 'Content-Type'=>$contentType]);
				header('HTTP/1.1 415 Unsupported Media Type');
				exit;
			}

			/*
			 * Lit des données json
			 * Si les données ne sont pas valides => HTTP 400
			 */
			$post = json_decode($entityBody, true);
			if($post === null or empty($post)) {
				$this->_container['log']->warning('Post data missing', ['__METHOD__'=>__METHOD__, 'Content-Type'=>$contentType]);
				header('HTTP/1.1 400 Bad request');
				exit;
			}

		}

		elseif (!empty($_POST)) {
			$this->_container['log']->warning('Post data are send throught $_POST variable not in body ', ['__METHOD__'=>__METHOD__]);
			$post = $_POST;
		}

		else {
			$this->_container['log']->warning('400 Bad Request, could not create workout', ['__METHOD__'=>__METHOD__]);
			header('HTTP/1.1 400 Bad request');
			exit;
		}

		/*
		 * Création de l'objet workout
		 * Vérfication des données
		 * Sauvegarde du workout
		 */
		$w = new \SylvainLG\Training\Model\Workout();

		$filters = [
			'title' => FILTER_SANITIZE_STRING,
			'date' => [	'filter' => FILTER_VALIDATE_REGEXP,
						'options' => [ 'regexp' => '#[0-9]{4}-[0-9]{2}-[0-9]{2}#' ]
					],
			'type' => FILTER_SANITIZE_STRING,
			'content' => FILTER_SANITIZE_STRING,
			// TODO filtrer les autres entrées de workout ! 
			'notes' => FILTER_SANITIZE_STRING,
			'duration' => FILTER_SANITIZE_STRING, // TODO c'est une heure, vérifier le format pour une meilleure sécu
			'difficulty' => [
				'filter' => FILTER_VALIDATE_INT,
				'options' => ['default'=>'', 'min_range'=>1, 'max_range'=>10],
			],
			'steps' => [
				'filter' 	=> FILTER_CALLBACK,
				'flags' 	=> FILTER_FORCE_ARRAY,
				'options'	=> function($arr) { return $arr; }
			],
		];

		$filtered_post = filter_var_array($post, $filters);

		return $filtered_post;
	}

}