<?php

namespace SylvainLG\Training\Service;

use \SylvainLG\Training\Model\Planning;

/**
 * Class Planning
 *
 * Permet de manipuler le planning entraînement
 */

class PlanningService extends \SylvainLG\Training\AbstractService {


	private $debug = false;

	/**
	 * Constructeur
	 */
	public function __construct($c) {
		parent::__construct($c);
	}

	/**
	 * Retourne tout le planning
	 */
	public function all() {
		$this->_log->info('all', ['__METHOD__'=>__METHOD__, 'filter'=>['athlete.id' => $this->_container['athlete_id']]]);
		return $this->_container['mysql']->query('SELECT * FROM planning')->fetchAll(\PDO::FETCH_CLASS, Planning::class,  ['hydrate'=>true]);
	}

	/**
	 * Retourne la période d'entraînement voulue
	 * 
	 * @return mixed planning or null
	 */
	public function get($uuid) {
		$this->_log->info('get '. $uuid, ['__METHOD__'=>__METHOD__]);
		
		$sth = $this->_container['mysql']->prepare(
			'SELECT * FROM planning WHERE uuid=? AND athlete=? LIMIT 1');
		$sth->execute([
			$uuid,
			$this->_container['athlete_id']
		]);

		return $sth->fetchObject(Planning::class, ['hydrate'=>true]) ?? null;
	}

	/**
	 * 
	 */
	public function count(array $filter = [], array $options = []) {

		$this->_log->info('get', ['__METHOD__'=>__METHOD__, 'filter' => $filter]);


		return $this->_container['mysql']->query(
			'SELECT count(id) FROM planning'
		)->fetch(\PDO::FETCH_BOTH)[0];

	}


	/**
	 * Ajoute une période au planning d'entraînement
	 *
	 * @param \SylvainLG\Training\Model\Planning planning 
	 * @return this
	 */
	public function add(Planning $w) {
		$this->_log->info('add', ['__METHOD__'=>__METHOD__, $w]);
		
		$w->athlete = intval($this->_container['athlete_id']);

		$sth = $this->_container['mysql']->prepare(
			'INSERT INTO planning(uuid, name, start, end, athlete) '.
			' VALUES(?,?,?,?,?)'
		);
		
		$res = $sth->execute([
			$w->uuid,
			$w->name,
			$w->start,
			$w->end,
			$w->athlete
		]);

		if(!$res) {
			$this->_log->error('Cannot store planning', ['__METHOD__'=>__METHOD__, $w,  $sth->errorInfo()]);
			throw new \Exception('Cannot store planning');
		}

		return $this;
	}

	/**
	 * Modification d'une période d'entraînement
	 */
	public function set(Planning $w) {
		$this->_log->info('set', ['__METHOD__'=>__METHOD__, $w]);
		
		$w->athlete = $this->_container['athlete_id'];
		
		$sth = $this->_container['mysql']->prepare(
			'UPDATE planning SET name=?, start=?, end=? '.
			' WHERE uuid=? AND athlete=?'
		);
		$res = $sth->execute([
			$w->name,
			$w->start,
			$w->end,
			$w->uuid,
			$w->athlete
		]);

		if(!$res) {
			$this->_log->error('Cannot store planning', ['__METHOD__'=>__METHOD__, $w]);
			throw new Excetion('Impossible de mettre à jour le Planning');
		}

		return $this;

	}

	/**
	 * Suppression d'une période du planning
	 * 
	 * @param Planning
	 * @return boolean
	 */
	public function delete(Planning $w) {
		$this->_log->info('set', ['__METHOD__'=>__METHOD__, $w]);

		$sth = $this->_container['mysql']->prepare(
			'DELETE FROM planning WHERE uuid=? AND athlete=?'
		);

		$res = $sth->execute([
			$w->uuid,
			$w->athlete
		]);

		if(!$res) {
			$this->_log->error('Cannot delete planning', ['__METHOD__'=>__METHOD__, $w]);
			throw new Excetion('Impossible de supprimer le planning');
		}

		return $this;
	}

}

