<?php

namespace SylvainLG\Training\Service;

use \SylvainLG\Training\Model\Event;

/**
 * Class Event
 *
 * Permet de manipuler les entraînements
 */

class EventService extends \SylvainLG\Training\AbstractService {

	/**
	 * Constructeur
	 */
	public function __construct($c) {
		parent::__construct($c);
	}

	/**
	 * Retourne tous les entraînements
	 */
	public function all() {
		$this->_log->info('all', ['__METHOD__'=>__METHOD__, 'filter'=>['athlete.id' => $this->_container['athlete_id']]]);
		return $this->_container['db']->event->find(['athlete.id' => $this->_container['athlete_id']], ['sort'=> ['date'=>-1]]);
	}

	/**
	 * Retourne l'entraînement voulu
	 * 
	 * @return mixed event or null
	 */
	public function get($uuid) {
		$this->_log->info('get '. $uuid, ['__METHOD__'=>__METHOD__]);
		return $this->_container['db']->event->findOne([
			'uuid'=> $uuid,
			'athlete.id' => $this->_container['athlete_id']
		]);
	}

	/**
	 * Renvoie la liste des activités
	 * 
	 * @param $filter Filtre respectant le format https://github.com/clue/json-query-language/blob/master/SYNTAX.md#combinators
	 */
	 public function filter(array $filter = [], array $options = []) {

		$this->_log->info('get', ['__METHOD__'=>__METHOD__, 'filter' => $filter]);

		$filter['athlete.id'] = $this->_container['athlete_id'];
		$data = $this->_container['db']->event->find($filter, $options);
		return $data;

	}

	/**
	 * 
	 */
	public function count(array $filter = [], array $options = []) {

		$this->_log->info('get', ['__METHOD__'=>__METHOD__, 'filter' => $filter]);

		$filter['athlete.id'] = $this->_container['athlete_id'];
		$data = $this->_container['db']->event->count($filter, $options);
		return $data;

	}


	/**
	 * Ajoute un entraînement à la liste
	 *
	 * @param \SylvainLG\Training\Model\Event event 
	 * @return this
	 */
	public function add(Event $e) {
		$this->_log->info('add', ['__METHOD__'=>__METHOD__, $e]);
		
		$e->athlete = ['id' => $this->_container['athlete_id']];
		$res = $this->_container['db']->event->insertOne($e);

		if($res->getInsertedCount() < 1) {
			throw new Excetion('Impossible de mettre à jour le Event');
		}

		return $this;
	}

	/**
	 * Modification d'un entraînement
	 */
	public function set(Event $e) {
		$this->_log->info('set', ['__METHOD__'=>__METHOD__, $e]);

		$e->athlete = ['id' => $this->_container['athlete_id']];
		
		$res = $this->_container['db']->event->replaceOne(
			[
				'uuid'=>$e->uuid,
				'athlete.id' => $this->_container['athlete_id']
			],
			$e,
			[
				'upsert' => true,
			]
		);

		if($res->getModifiedCount() < 1) {
			throw new Excetion('Impossible de mettre à jour le Event');
		}

		return $this;

	}

	/**
	 * Suppression d'un event
	 * 
	 * @param Event
	 * @return boolean
	 */
	public function delete(Event $e) {
		$this->_log->info('set', ['__METHOD__'=>__METHOD__, $e]);

		$res = $this->_container['db']->event->deleteOne([
			'uuid'=>$e->uuid,
			'athlete.id' => $this->_container['athlete_id']
		]);

		if($res->getDeletedCount() < 1) {
			throw new Excetion('Impossible de supprimer le Event');
		}

		return $this;
	}

}

