<?php

namespace SylvainLG\Training\Console;

abstract class AbstractCommand implements \SylvainLG\Training\ContainerAwareInterface {

	use \SylvainLG\Training\ContainerAwareTrait;

	public function command($method) {
		$this->_container['log']->info('command', ['__METHOD__'=>__METHOD__]);
		
		// var_dump($_SERVER['argv']);
		// var_dump($method);

		$methodName = $method.'Action';

		if(method_exists($this, $methodName)) {
			$this->_container['log']->info('Call sub-command', ['__METHOD__'=>__METHOD__]);
			$this->$methodName(array_slice($_SERVER['argv'], 1));
		} else {
			$this->_container['log']->error('Sub command not found', ['__METHOD__'=>__METHOD__]);
		}

	}

}