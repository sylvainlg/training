<?php

namespace SylvainLG\Training\Model;

use Ramsey\Uuid\Uuid;

/**
 * Classe de données d'un événements
 * 
 * Représente les événements extérieurs au monde du vélo (rdv médicaux, contraintes récupération - travail, réunion, déplacement-, ...)
 */

class Planning implements \JsonSerializable {

	private $uuid;
	private $name;
	private $start;
	private $end;
	private $athlete;
	
	public function __construct($hydrate = false) {
		if($hydrate !== true) {
			$this->uuid = 'planning_' . Uuid::uuid4()->toString();
		}
	}

	public function __get($key) {
		return $this->{$key};
	}

	public function __set($key, $value) {
		$this->{$key} = $value;
		return $this;
	}

	public function __isset($key) {
		return array_key_exists($key, get_object_vars($this));
	}

	public function fromArray($arr) {

		if(!isset($arr['uuid']) or empty($arr['uuid'])) {
			throw new \Exception('UUID missing');
		}

		$this->uuid = $arr['uuid'];
		$this->name = $arr['name'];
		$this->start = $arr['start'];
		$this->end = $arr['end'];
		$this->athlete = intval($arr['athlete']);
		
		return $this;

	}

	public function jsonSerialize() {
		return get_object_vars($this);
	}

	public function equalsTo(Planning $slot) {
		return $this->uuid === $slot->uuid;

	}

	/*
	 *	CUSTOM LOGIC
	 */

	/**
	 * Retourne le nombre de jours sur lequel le slot se déroule
	 */
	public function duration() {
		return (new \Datetime($this->end))->diff((new \Datetime($this->start)), true)->days;
	}

	/**
	 * Retourne le "poids" du slot sur une semaine
	 *  Le calcul commence à la date passée en paramètre.
	 */
	public function weight(\Datetime $dt) {
		return 0;
	}

}
