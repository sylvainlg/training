<?php

namespace SylvainLG\Training;

use \Phroute\Phroute\HandlerResolverInterface;

class RouterResolver implements HandlerResolverInterface
{
    private $_container;

    public function __construct(\Pimple\Container $container)
    {
        $this->_container = $container;
    }

    public function resolve($handler)
    {
        /*
         * Only attempt resolve uninstantiated objects which will be in the form:
         *
         *      $handler = ['App\Controllers\Home', 'method'];
         */
        if(is_array($handler) and is_string($handler[0]))
        {
            $handler[0] = new $handler[0];
			if($handler[0] instanceof \SylvainLG\Training\ContainerAwareInterface) {
				$handler[0]->setContainer($this->_container);
			}
        }

        return $handler;
    }

	private function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}

}
