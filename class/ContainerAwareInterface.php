<?php

namespace SylvainLG\Training;

interface ContainerAwareInterface {
	public function setContainer(\Pimple\Container $container);
}