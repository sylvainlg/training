<?php
declare(strict_types=1);
namespace SylvainLG\Training;

use PHPUnit\Framework\TestCase;

use Pimple\Container;

define('APPDIR', realpath(__DIR__ . '/../../../'));

/**
 * @cover AppConfig
 */
final class AppConfigTest extends TestCase {

	private static $_CONFDIR = APPDIR . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;

	public function testCanAccessConfigDirectory() {

		$this->assertDirectoryExists(self::$_CONFDIR);
	}

	public function testCanAccessConfigFile() {

		$this->assertFileExists(self::$_CONFDIR . 'config.json');
	}

	public function testCanCreateAppConfig() {

		$this->assertInstanceOf(
			AppConfig::class,
			new AppConfig()
		);

	}

}